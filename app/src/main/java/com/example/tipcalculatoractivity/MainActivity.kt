package com.example.tipcalculatoractivity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        calc_btn.setOnClickListener {
            handleClick()
        }
    }

    private fun handleClick(){

        try {
            val tipInputStr = enter_tip.editableText.toString()

            Toast.makeText(this,"Calculating", Toast.LENGTH_SHORT).show()

            val selectedRadioButton = radio_group.checkedRadioButtonId

            val tipInputNumber = tipInputStr.toDouble()

            val r : Double?

            r = when (selectedRadioButton){
                R.id.radio1 -> .10
                R.id.radio2 -> .20
                R.id.radio3 -> .30
                else -> throw Exception ("Invalid Selection")
            }

            //val tipamt = tip(tipInputNumber,r)

            val total = tip(tipInputNumber,r) + tipInputNumber

            result_tv.text = tip(tipInputNumber,r).toString()

            total_tv.text = total.toString()

        }catch (e: Exception){

        }

    }

    private fun tip(a: Double , b: Double): Double{
        return a * b

    }
}